#!/bin/bash
# Maintainer: Nico Meisenzahl <nico@meisenzahl.org>
# Not intended for production!

# Customize based on your environment

# Disable colors
export BLUEMIX_COLOR=false

# Display cluster info
ibmcloud ks clusters
ibmcloud ks workers soccnx_cluster

# Configure kubectl
ibmcloud ks cluster-config soccnx_cluster
export KUBECTL=$(ibmcloud ks cluster-config soccnx_cluster |tail -n 1)
eval $KUBECTL
kubectl get nodes

# helm init
helm init --upgrade
